﻿using System;
using System.Threading.Tasks;
using JubloAps.MessageEvent.Contracts.Types;
using JubloAps.MessageEvent.Library.Managers;
using JubloAps.Shared.Aspects;
using JubloAps.Shared.Interfaces;
using PostSharp.Patterns.Contracts;

namespace JubloAps.MessageEvent.Library
{
    public abstract class MessageEventFacilitator
    {
        public async Task SubmitAsync([GuidNotNull] Guid caseId,[NotEmpty] string apiKey,[NotNull] MessageEventType messageEventType,[NotEmpty] string message)
        {
            var messageEventManager = new MessageEventManager(this.GetQueueConfig());
            await messageEventManager.SubmitAsync(caseId, apiKey,  messageEventType, message);
        }

        public abstract IQueueConfig GetQueueConfig();
    }
}
