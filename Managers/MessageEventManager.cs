﻿using System;
using System.Threading.Tasks;
using JubloAps.MessageEvent.Contracts;
using JubloAps.MessageEvent.Contracts.Types;
using JubloAps.MessageEvent.Transmitter;
using JubloAps.Shared.Interfaces;

namespace JubloAps.MessageEvent.Library.Managers
{
    internal class MessageEventManager
    {
        private readonly IQueueConfig _queueConfig;

        public MessageEventManager(IQueueConfig queueConfig)
        {
            this._queueConfig = queueConfig;
        }

        public async Task SubmitAsync(Guid caseId, string apiKey, MessageEventType messageEventType, string message)
        {
            var messageEventTransmitterFacilitator = new MessageEventTransmitterFacilitator(this._queueConfig);
            var messageEventContracts = new MessageEventContract
            {
                CaseId = caseId,
                MessageEventType = messageEventType,
                Message = message,
                ApiKey = apiKey
            };
            await messageEventTransmitterFacilitator.TransmitAsync(messageEventContracts);
        }
    }
}